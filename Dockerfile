FROM python:3.10

WORKDIR /app
COPY . .

RUN apt install make
RUN pip install -r requirements.txt

CMD ["make", "run"]
