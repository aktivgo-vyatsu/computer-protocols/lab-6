import os
import telebot

from telebot import types
from schedule.parser import TeacherScheduleParser

token = os.getenv('BOT_TOKEN')

bot = telebot.TeleBot(token)

parser = TeacherScheduleParser()

def error(message, err):
    print(f'error: {err}')
    bot.send_message(message.chat.id, 'Произошла внутрення ошибка, повторите действия заново /start',
                     reply_markup=types.ReplyKeyboardRemove())


@bot.message_handler(commands=['start', 'help'])
def select_institute(message):
    parser.reset()

    bot.send_message(message.chat.id,
                     'Привет, это бот для просмотра расписания преподавателей ВЯТгу\n'
                     'Подождите пока загрузится список институтов',
                     reply_markup=types.ReplyKeyboardRemove(),
                     )

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)

    try:
        institutes = parser.parse_institutes()
    except Exception as err:
        return error(message, err)

    for institute in institutes:
        markup.add(types.KeyboardButton(institute))

    bot.send_message(message.chat.id, 'Выберите институт', reply_markup=markup)
    bot.register_next_step_handler(message, select_faculty)


def select_faculty(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)

    try:
        faculties = parser.parse_faculties(message.text)
    except Exception as err:
        return error(message, err)

    for faculty in faculties:
        markup.add(types.KeyboardButton(faculty))

    bot.send_message(message.chat.id, 'Выберите факультет', reply_markup=markup)
    bot.register_next_step_handler(message, select_department)


def select_department(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)

    try:
        departments = parser.parse_departments(message.text)
    except Exception as err:
        return error(message, err)

    for department in departments:
        markup.add(types.KeyboardButton(department))

    bot.send_message(message.chat.id, 'Выберите кафедру', reply_markup=markup)
    bot.register_next_step_handler(message, select_period)


def select_period(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)

    try:
        periods = parser.parse_periods(message.text)
    except Exception as err:
        return error(message, err)

    for period in periods:
        markup.add(types.KeyboardButton(period))

    bot.send_message(message.chat.id, 'Выберите период', reply_markup=markup)
    bot.register_next_step_handler(message, select_day)


days = list()


def select_day(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)

    global days

    try:
        days = parser.parse_days(message.text)
    except Exception as err:
        return error(message, err)

    for day in days:
        markup.add(types.KeyboardButton(day))

    bot.send_message(message.chat.id, 'Выберите день', reply_markup=markup)
    bot.register_next_step_handler(message, select_teacher)


def select_teacher(message):
    parser.selected_day = days.index(message.text)

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)

    try:
        teachers = parser.parse_teachers()
    except Exception as err:
        return error(message, err)

    for teacher in teachers:
        markup.add(types.KeyboardButton(teacher))

    bot.send_message(message.chat.id, 'Выберите преподавателя', reply_markup=markup)
    bot.register_next_step_handler(message, view_schedule)


def view_schedule(message):
    try:
        schedule = parser.parse_schedule(message.text)
    except Exception as err:
        return error(message, err)

    reply = '--------------------------\n'

    if len(schedule) == 0:
        reply = 'Пар нет'
    else:
        for k, v in schedule.items():
            reply += f'{k}\n{v}\n--------------------------\n'

    bot.send_message(message.chat.id, reply, reply_markup=types.ReplyKeyboardRemove())
    bot.send_message(message.chat.id, 'Для просмотра другого расписания введите /start')


@bot.message_handler(content_types=['text'])
def get_text_messages(message):
    bot.send_message(message.chat.id, 'Для начала работы введите /start', reply_markup=types.ReplyKeyboardRemove())


bot.infinity_polling()
