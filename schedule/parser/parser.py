import re

import requests

from bs4 import BeautifulSoup


def escape_characters(text: str) -> str:
    return text.replace('(', '\(').replace(')', '\)')


class TeacherScheduleParser:
    def __init__(self):
        self.current_resource = None
        self.base_url = None
        self.soup_registry = dict()
        self.schedule_link_registry = dict()

        self.selected_day = None

        self.reset()

    def reset(self):
        self.base_url = 'https://www.vyatsu.ru'
        self.current_resource = '/studentu-1/spravochnaya-informatsiya/teacher.html'
        self.schedule_link_registry = dict()
        self.selected_day = None

    def __get_resource_url(self, resource: str) -> str:
        return self.base_url + resource

    def __get_soup_instance(self, url: str) -> BeautifulSoup:
        if url in self.soup_registry:
            return self.soup_registry[url]
        instance = BeautifulSoup(requests.get(url).content, 'html.parser')
        self.soup_registry[url] = instance
        return instance

    def __get_schedule_link(self, period: str) -> str:
        if period not in self.schedule_link_registry:
            raise BrokenPipeError(f'period {period} not exists')
        return self.schedule_link_registry[period]

    def parse_institutes(self) -> list:
        result = []
        soup = self.__get_soup_instance(self.__get_resource_url(self.current_resource))
        for institute in soup.find_all('div', {'class': 'headerEduPrograms'}):
            institute_title = institute.text.replace('(ОРУ)', '').strip()
            result.append(institute_title)
        return result

    def parse_faculties(self, institute: str) -> list:
        result = []
        soup = self.__get_soup_instance(self.__get_resource_url(self.current_resource))
        institute = escape_characters(institute)
        faculties = soup.find('div', text=re.compile(f'.*{institute}.*')).find_next('table').findChildren('div', {
            'class': 'fak_name'})
        for faculty in faculties:
            faculty_title = faculty.text.replace('(ОРУ)', '').strip()
            result.append(faculty_title)
        return result

    def parse_departments(self, faculty: str) -> list:
        result = []
        soup = self.__get_soup_instance(self.__get_resource_url(self.current_resource))
        faculty = escape_characters(faculty)
        departments = soup.find('div', {'class': 'fak_name'}, text=re.compile(f'.*{faculty}.*')).find_next(
            'table').findChildren('div', {
            'class': 'kafPeriod'})
        for department in departments:
            department_title = department.text.replace('(ОРУ)', '').strip()
            result.append(department_title)
        return result

    def parse_periods(self, department: str) -> list:
        result = []
        soup = self.__get_soup_instance(self.__get_resource_url(self.current_resource))
        department = escape_characters(department)
        schedule_intervals = soup.find('div', text=re.compile(f'.*{department}.*')).find_next('div').findChildren('a',
                                                                                                                  text=re.compile(
                                                                                                                      '.'))
        schedule_intervals.reverse()
        for schedule in schedule_intervals:
            schedule_interval_title = schedule.text.strip()
            result.append(schedule_interval_title)
            self.schedule_link_registry[schedule_interval_title] = schedule['href']
        return result

    def parse_days(self, period: str) -> list:
        result = []
        self.current_resource = self.__get_schedule_link(period)
        period = escape_characters(period)
        soup = self.__get_soup_instance(self.__get_resource_url(self.current_resource))
        start_index = 2
        interval = 7
        rows = soup.find_all('tr')[start_index:]
        for i in range(len(rows) // interval):
            result.append(rows[i * interval].findChildren('td', {'rowspan': 7})[0].text.replace('\xa0', ' ').strip())
        return result

    def parse_teachers(self) -> list:
        result = []
        soup = self.__get_soup_instance(self.__get_resource_url(self.current_resource))
        rows = soup.find_all('tr')
        column_titles = [column_title.text.strip() for column_title in rows[1].findChildren('span')][2:]
        for teacher_name in column_titles:
            result.append(teacher_name.replace('\xa0', ' ').strip())
        return result

    def parse_schedule(self, teacher: str) -> dict:
        result = {}
        soup = self.__get_soup_instance(self.__get_resource_url(self.current_resource))
        teacher = escape_characters(teacher)
        start_index = 2
        interval = 7
        rows = soup.find_all('tr')[start_index:]
        teacher_index = self.parse_teachers().index(teacher)

        selected_day_row = self.selected_day * interval

        for i in range(0, interval):
            selected_day_classes = rows[selected_day_row + i if i != 0 else selected_day_row].findChildren('td')
            time = selected_day_classes[0 if i != 0 else 1].text.replace('\xa0', ' ').strip()
            subject = selected_day_classes[teacher_index + 1 if i != 0 else teacher_index + 2].text.replace('\xa0',
                                                                                                            ' ').strip()
            if subject != '':
                result[time] = subject
        return result
